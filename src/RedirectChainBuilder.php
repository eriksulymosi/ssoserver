<?php

namespace ErikSulymosi\SSOServer;

use Lcobucci\JWT\Parser;
use Laravel\Passport\Passport;
use Laminas\Diactoros\Response;
use Laravel\Passport\Bridge\User;
use Illuminate\Support\Facades\URL;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Auth;
use Laminas\Diactoros\ServerRequest;
use Laravel\Passport\TokenRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use League\OAuth2\Server\AuthorizationServer;
use Illuminate\Contracts\Auth\Authenticatable;
use Laravel\Passport\Http\Controllers\HandlesOAuthErrors;

class RedirectChainBuilder
{
    use HandlesOAuthErrors;

    protected $encrypter = null;
    protected $server = null;
    protected $tokens = null;
    protected $jwt = null;

    public function __construct(
        AuthorizationServer $server,
        TokenRepository $tokens,
        Parser $jwt
    ) {
        $this->server = $server;
        $this->tokens = $tokens;
        $this->jwt = $jwt;
    }

    public function toLogin(string $target = null, string $default = null)
    {
        $default = $default ? URL::to($default) : Session::pull('url.intended', URL::to('/'));

        $target = $target ?? $default;
        $user = Auth::user();

        if (! $user instanceof Authenticatable) {
            return Redirect::back();
        }

        $redirectTo = Passport::client()
            ->autoLogin()
            ->get()
            ->reduce(function ($carry, $client) use ($user) {
                $payload = SSO::payloadCallback() ?? function () use ($client, $user) {
                    return $this->getAuthorizationCode($client, $user->getAuthIdentifier(), ['*']);
                };

                $encrypter = new Encrypter(substr($client->secret, 0, 32), Config::get('app.cipher'));

                return sprintf(
                    '%s?payload=%s&redirect=%s',
                    rtrim($client->sso_redirect_uri, '/'),
                    urlencode($encrypter->encrypt($payload($client), false)),
                    urlencode($carry)
                );
            }, $target);

        return Redirect::away($redirectTo);
    }

    public function toLogout(string $target = null, string $default = null)
    {
        $default = $default ? URL::to($default) : URL::to('/');

        $target = Session::pull('url.intended', $target ?? $default);

        $redirectTo = Passport::client()
            ->autoLogin()
            ->get()
            ->reduce(function ($carry, $client) {
                $encrypter = new Encrypter(
                    substr($client->secret, 0, 32),
                    Config::get('app.cipher')
                );

                return sprintf(
                    '%s/logout/%s?redirect=%s',
                    rtrim($client->sso_redirect_uri, '/'),
                    $encrypter->encrypt($client->secret, false),
                    urlencode($carry)
                );
            }, $target);

        return Redirect::away($redirectTo);
    }

    protected function getAuthorizationCode($client, $userId, array $scopes)
    {
        $authRequest = $this->getAuthRequest($client, $userId, $scopes);

        $authResponse = $this->getAuthCodeResponse($authRequest, $userId);

        return $this->getAuthCodeFromResponse($authResponse);
    }

    protected function getAuthRequest($client, $userId, array $scopes)
    {
        return $this->withErrorHandling(function () use ($client, $userId, $scopes) {
            return $this->server->validateAuthorizationRequest(
                $this->createRequest($client, $scopes)
            );
        });
    }

    protected function getAuthCodeResponse($authRequest, $userId)
    {
        $authRequest->setUser(new User($userId));

        $authRequest->setAuthorizationApproved(true);

        return $this->withErrorHandling(function () use ($authRequest) {
            return $this->server->completeAuthorizationRequest($authRequest, new Response);
        });
    }

    protected function getAuthCodeFromResponse($response)
    {
        $locationHeader = $response->getHeaderLine('location');
        $urlQuery = parse_url($locationHeader, PHP_URL_QUERY);
        parse_str($urlQuery, $parsedQuery);

        return $parsedQuery['code'];
    }

    protected function createRequest($client, array $scopes)
    {
        $redirectURI = explode(',', $client->redirect);

        return (new ServerRequest)->withQueryParams([
            'response_type' => 'code',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'redirect_uri' => array_shift($redirectURI),
            'scope' => implode(',', $scopes),
        ]);
    }
}
