<?php

namespace ErikSulymosi\SSOServer\Models;

use Laravel\Passport\Client as BaseClient;

class Client extends BaseClient
{
    public function __construct(array $attributes = [])
    {
        $this->casts['first_party_client'] = 'boolean';
        $this->casts['domains'] = 'array';

        parent::__construct($attributes);
    }

    public function scopeFirstParty($query, bool $state = true)
    {
        return $query->whereFirstPartyClient($state);
    }

    public function scopeAutoLogin($query)
    {
        return $query->whereNotNull('sso_redirect_uri');
    }

    public function skipsAuthorization()
    {
        return $this->first_party_client;
    }
}
