<?php

namespace ErikSulymosi\SSOServer;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register sso server services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadMigrationsFrom(__DIR__.'/../migrations');
    }

    /**
     * Bootstrap sso server services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../migrations/' => database_path('migrations')
            ], 'sso-server-migrations');
        }
    }
}
