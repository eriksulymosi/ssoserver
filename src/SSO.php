<?php

namespace ErikSulymosi\SSOServer;

use Closure;
use Illuminate\Support\Facades\Route;

class SSO
{
    protected static $payloadCallback;

    public static function routes($callback = null, array $options = [])
    {
        $callback = $callback ?: function ($router) {
            $router->all();
        };

        $defaultOptions = [
            'prefix' => 'sso',
            'namespace' => '\ErikSulymosi\SSOServer\Http\Controllers',
        ];

        $options = array_merge($defaultOptions, $options);

        Route::group($options, function ($router) use ($callback) {
            $callback(new RouteRegistrar($router));
        });
    }

    public static function payloadCallback(Closure $callback = null)
    {
        if ($callback === null) {
            return self::$payloadCallback;
        }

        self::$payloadCallback = $callback;
    }
}
