<?php

namespace ErikSulymosi\SSOServer;

use Illuminate\Contracts\Routing\Registrar as Router;

class RouteRegistrar
{
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function all()
    {
        $this->forJWK();
    }

    public function forJWK()
    {
        $this->router->get('jwk.json', [
            'uses' => 'JWKController@getPublicKey',
            'as' => 'sso.jwk.public',
        ]);
    }
}
