<?php

namespace ErikSulymosi\SSOServer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers as BaseRegistersUsers;

trait RegistersUsers
{
    use RedirectsUsers;
    use BaseRegistersUsers;

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        return $this->redirectResponseToLogin($request);
    }
}
