<?php

namespace ErikSulymosi\SSOServer\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use ErikSulymosi\SSOServer\RedirectChainBuilder;

trait RedirectsUsers
{
    protected function redirectResponseToLogin(Request $request, string $default = null)
    {
        $redirector = App::make(RedirectChainBuilder::class);

        if (($referer = $this->getUrl($request)) !== null) {
            parse_str(parse_url($referer, PHP_URL_QUERY) ?? '', $queryParams);

            if (($redirectTo = Arr::get($queryParams, 'redirect')) !== null && filter_var($redirectTo, FILTER_VALIDATE_URL)) {
                return $redirector->toLogin($redirectTo);
            }
        }

        return $redirector->toLogin(null, $default);
    }

    protected function redirectResponseToLogout(Request $request, string $default = null)
    {
        $redirector = App::make(RedirectChainBuilder::class);

        if (($referer = $this->getUrl($request)) !== null) {
            parse_str(parse_url($referer, PHP_URL_QUERY) ?? '', $queryParams);

            if (($redirectTo = Arr::get($queryParams, 'redirect')) !== null && filter_var($redirectTo, FILTER_VALIDATE_URL)) {
                return $redirector->toLogout($redirectTo);
            }
        }

        return $redirector->toLogout(null, $default);
    }

    protected function getUrl(Request $request)
    {
        return $request->has('redirect') ? $request->fullUrl() : $request->header('referer');
    }
}
