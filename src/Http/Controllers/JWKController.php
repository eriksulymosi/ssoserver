<?php

namespace ErikSulymosi\SSOServer\Http\Controllers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Config;
use Jose\Component\KeyManagement\JWKFactory;

class JWKController
{
    public function getPublicKey()
    {
        $key = str_replace(
            '\\n',
            "\n",
            Config::get('passport.public_key')
        );

        if (! $key) {
            $key = file_get_contents('file://' . Passport::keyPath('oauth-public.key'));
        }

        return JWKFactory::createFromKey($key);
    }
}
