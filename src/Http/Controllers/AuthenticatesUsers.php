<?php

namespace ErikSulymosi\SSOServer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers as BaseAuthenticatesUsers;

trait AuthenticatesUsers
{
    use RedirectsUsers;
    use BaseAuthenticatesUsers {
        BaseAuthenticatesUsers::logout as baseLogout;
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return $this->redirectResponseToLogin($request);
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        return $this->redirectResponseToLogout($request);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->revokeOldTokens($request->user());

        return $this->baseLogout($request);
    }

    protected function revokeOldTokens($user)
    {
        if ($user !== null) {
            $user->tokens()->whereHas('client', function($query) {
                $query->autoLogin();
            })->get()->each->revoke();
        }
    }
}
