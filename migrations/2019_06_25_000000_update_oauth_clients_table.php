<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOauthClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oauth_clients', function (Blueprint $table) {
            $table->boolean('first_party_client')->default(false)->after('password_client');
            $table->string('sso_redirect_uri')->nullable()->after('first_party_client');
            $table->string('domains', 1024)->nullable()->after('sso_redirect_uri');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oauth_clients', function (Blueprint $table) {
            $table->dropColumn('first_party_client');
            $table->dropColumn('sso_redirect_uri');
            $table->dropColumn('domains');
        });
    }
}
